-- Module à charger pour l'exercice sur les fonctions

-- Des matrices
matriceA = [[ 3,  1, -2],
            [-1,  0,  2],
            [ 4, -1,  5]]
matriceB = [[-1,  4,  2],
            [ 0, -6, -1],
            [ 8,  6, -4]]
