# Programmation fonctionnelle

Ce dépôt contient des fichiers utilisés lors de l'atelier intitulé
« Programmation fonctionnelle », tenu le 3 juin 2019, de 9h à 12h, dans le
cadre du camp mathématique de l'Université du Québec à Trois-Rivières.
L'atelier est animé par Alexandre Blondin Massé.

## Diapositives

Les diapositives utilisées sont disponibles dans le fichier
[`programmation-fonctionnelle.pdf`](./programmation-fonctionnelle.pdf).

## Références

## Fonctions utiles sur les listes

Quelques fonctions, avec leur signature, qui pourraient vous être utiles:

```haskell
-- Fonctions de base
(++) :: [a] -> [a] -> [a]                -- Concaténation de deux listes
head :: [a] -> a                         -- Premier élément d'une liste
last :: [a] -> a                         -- Dernier élément d'une liste
tail :: [a] -> [a]                       -- Queue d'une liste
init :: [a] -> [a]                       -- Partie initiale d'une liste
null :: [a] -> Bool                      -- Indique si une liste est vide
length :: [a] -> Int                     -- Longueur d'une liste
replicate :: Int -> a -> [a]             -- Répète un élément plusieurs fois dans une liste
(!!) :: [a] -> Int -> a                  -- Retourne le i-ème élément d'une liste

-- Sous-listes
take :: Int -> [a] -> [a]                -- Prend le préfixe d'une liste de longueur donnée
drop :: Int -> [a] -> [a]                -- Prend le suffixe d'une liste en supprimant le nombre
                                         -- donné d'éléments au début

-- Transformations
map :: (a -> b) -> [a] -> [b]            -- Applique une fonction à chaque élément d'une liste
filter :: (a -> b) -> [a] -> [b]         -- Ne conserve que les éléments d'une liste vérifiant une condition
reverse :: [a] -> [a]                    -- Renverse une liste
intersperse :: a -> [a] -> [a]           -- Insère des copies d'un élément entre
                                         -- chacun des éléments d'une liste
intercalate :: [a] -> [[a]] -> [a]       -- Insère des copies d'une liste entre
                                         -- chacun des listes d'une liste
transpose :: [[a]] -> [[a]] -> [a]       -- Retourne la transposée d'une matrice
                                         -- (une matrice étant une liste de listes)
permutations :: [a] -> [[a]]             -- Retourne une liste de permutations
concat :: [[a]] -> [a]                   -- Aplatit une liste de listes en les concaténant
sort :: Ord a => [a] -> [a]              -- Trie une liste
nub :: Eq a => [a] -> [a]                -- Supprime les doublons d'une liste

-- Arithmétique
sum :: Num a => [a] -> a                 -- Retourne la somme des éléments d'une liste
product :: Num a => [a] -> a             -- Retourne le produit des éléments d'une liste
maximum :: Ord a => [a] -> a             -- Retourne le maximum d'une liste
minimum :: Ord a => [a] -> a             -- Retourne le minimum d'une liste

-- Prédicats
isPrefixOf :: Eq a => [a] -> [a] -> Bool -- Indique si une liste est préfixe d'une autre
isSuffixOf :: Eq a => [a] -> [a] -> Bool -- Indique si une liste est suffixe d'une autre
elem :: Eq a => a -> [a] -> Bool         -- Indique si un élément apparaît dans une liste
notElem :: Eq a => a -> [a] -> Bool      -- Indique si un élément n'apparaît pas dans une liste

-- Logique
and :: [a] -> a                          -- Retourne vrai si tous les éléments de la liste sont vrais
or :: [a] -> a                           -- Retourne vrai s'il existe un élément vrai dans la liste
all :: (a -> Bool) -> [a] -> a           -- Retourne vrai si tous les éléments de la liste
                                         -- vérifient une condition donnée
any :: (a -> Bool) -> [a] -> a           -- Retourne vrai s'il existe un élément de la liste
                                         -- vérifiant une condition donnée

-- Listes infinies
repeat :: a -> [a]                       -- Répète un élément à l'infini
cycle :: [a] -> [a]                      -- Répète les éléments d'une liste à l'infini
```

## Exercices (série 1)

### a - Manipulation de listes

Récupérez le fichier [ExerciceListes.hs](ExerciceListes.hs) disponible dans le
dépôt et chargez-le dans l'interpréteur Haskell à l'aide de la commande:

```haskell
:l ExerciceListes.hs
```
Aussi, chargez les modules `Data.List` et `Data.Char` en tapant
```haskell
import Data.Char
import Data.List
```

En utilisant les fonctions suggérées entre parenthèses, répondez aux questions
suivantes. *Remarque*: même s'il est possible de le faire autrement qu'en
utilisant les fonctions suggérées, je vous encourage à respecter les
contraintes, afin de mieux apprendre à utiliser certaines fonctions. Si vous ne
vous rappelez plus de ce que fait la fonction, il peut toujours être utile
d'étudier sa signature avec la commande
```
:t nomFonction
```
Aussi, essayez d'obtenir la réponse en écrivant l'expression en **une seule
ligne**.

- (`sort`) Triez la liste `nombres` (disponible dans `ExerciceListes.hs`).
- (`nub`) Transformez la liste `nombres` de sorte qu'elle ne contienne aucun
  doublon.
- (`minimum`, `maximum`) Calculez l'étendue de la liste `nombres`. En
  statistiques, l'*étendue* d'une liste de nombre est la différence entre la
  valeur maximale et la valeur minimale.
- (`odd`, `filter`, `take`) Extraire les 10 premiers nombres de la liste
  `nombres` qui sont impairs.
- (`mod`, `div`) Produisez le couple `(q,r)` où `q` est le quotient et `r` le
  reste de la division de `147457` par `1297`.
- (`sum`) Montrez que la somme des entiers de 0 à 100 est égale à `100 * 101 / 2`.
- (`length`, `permutations`, `factorial`) Montrez que le nombre de permutations
  de la liste `[1..8]` est bien égal à `8!` (le point d'exclamation se lit
  "factoriel").
- (`transpose`) Montrez que la matrice `matrice` est symétrique (les valeurs
  sont invariantes quand on applique une réflexion par rapport à la diagonale).
- (`toLower`, `map`) Transformez `phrase` en lettre minuscule.
- (`toLower`, `filter`, `map`, `isLower`) Transformez `phrase` en minuscule et
  en supprimant les espaces et les signes de ponctuation.
- (`toLower`, `filter`, `map`, `isLower`, `reverse`) Montrez que `phrase` est
  une phrase palindromique, c'est-à-dire qu'on obtient la même phrase qu'on la
  lise de gauche à droite ou de droite à gauche, en supposant qu'on ignore la
  casse et les signes de ponctuation.
- (`filter`, `isSpace`, `length`) Compter le nombre d'espaces dans `phrase`.
- (`filter`, `isUpper`, `length`) Compter le nombre de lettres majuscules dans
  `phrase`.
- (`all`, `any`, `take`, `length`, `drop`, `isUpper`, `isAlpha`, `isDigit`)
  Montrez que les codes permanents contenus dans la liste `codes` sont des
  codes permanent valide (exactement 4 lettres majuscules, suivies d'exactement
  8 chiffres). Montrez ensuite qu'aucun code permanent dans `codesErrones`
  n'est valide.

### b - La fonction pgcd

Implémentez la fonction qui calcule le plus grand commun diviseur de deux
entiers. Sa signature est
```haskell
pgcd :: Int -> Int -> Int
```
*Remarque*: Mathématiquement, *pgcd(0,0)* n'est pas défini, mais vous pouvez
convenir que *pgcd(0,0) = 0* pour simplifier.

### c - Périodes d'une liste

Soit `L` une liste de longueur `n` et `p` un entier tel que `0 < p < n`. On dit
que `p` est une *période* de `L` si `L[i] = L[i+p]` pour tout indice `i` valide
dans `L` tel que `L[i+p]` est aussi un indice valide.

Implémentez une fonction qui indique si un nombre est une période d'une liste,
dont la signature est
```haskell
estPeriode :: Eq a => Int -> [a] -> Bool
```
*Remarque*: Vous pouvez retourner systématiquement `False` si la période
indiquée n'est pas entre `1` et `n - 1`.

Par exemple, on s'attend au comportement suivant:
```haskell
Prelude> estPeriode 3 [1,2,3,1,2]
True
Prelude> estPeriode 0 [1,2,3,1,2,3]
False
Prelude> estPeriode 4 [1,2,3,1,1]
True
```

Ensuite, implémentez une fonction
```haskell
periodes :: Eq a => [a] -> [Int]
```
qui retourne la liste en ordre croissant de toutes les périodes d'une liste. On
s'attend donc au comportement suivant:
```haskell
Prelude> periodes [1,2,3,1,2]
[3]
Prelude> periodes "aaaaa"
[1,2,3,4]
Prelude> periodes [0,1,0,0,1,0,1,0,0,1,0]
[5,8,10]
```

## Exercices (série 2)

Récupérez le fichier [ExerciceFonctions.hs](ExerciceFonctions.hs) disponible
dans ce répertoire.

En utilisant les fonctions suggérées entre parenthèses, répondez aux questions
suivantes. *Remarque*: même s'il est possible de le faire autrement qu'en
utilisant les fonctions suggérées, je vous encourage à respecter les
contraintes, afin de mieux apprendre à utiliser certaines fonctions.

### a - Multiplication d'une matrice par un scalaire

(`map`, `.`, `(*5)`) Produisez la matrice obtenue de `matriceA` en multipliant
chaque entrée par `5`. Vous devriez donc obtenir
```haskell
[[15,5,-10],[-5,0,10],[20,-5,25]]
```

(`map`, `.`, `(*)`) Déduisez-en l'implémentation d'une fonction
`multiplicationParScalaire` qui multiplie toutes les entrées d'une matrice par
un scalaire donné, dont la signature est
```haskell
multiplicationParScalaire :: Num a => a -> [[a]] -> [[a]]
```

### b - Somme de deux matrices

(`zipWith`, `(+)`, `.`) Produisez la somme des matrices `matriceA` et
`matriceB`. Le résultat attendu est
```haskell
[[-1,4,2],[0,-6,-1],[8,6,-4]]
```

(`zipWith`, `(+)`, `.`) Déduisez-en une fonction
```haskell
sommeMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
```
qui calcule la somme de deux matrices.

### c - Produit scalaire de deux listes

(`sum`, `zipWith`, `(*)`) Définissez une fonction qui calcule le produit
scalaire de deux listes (pas besoin de valider si les longueurs sont égales,
vous pouvez supposer que c'est le cas). La signature est
```haskell
produitScalaire :: Num a => [a] -> [a] -> a
```
On s'attend à ce que le résultat soit le suivant
```haskell
Prelude> produitScalaire [1,2,3] [4,5,6]
32
Prelude> produitScalaire [-1,-3,2] [0,2,4]
2
```

### d - Produit de deux matrices

(`produitScalaire`, `transpose`) Définissez une fonction qui calcule le produit
de deux matrices, dont la signature est
```haskell
produitMatrices :: Num a => [[a]] -> [[a]] -> [[a]]
```
Il y a une façon élégante de faire en observant que le produit de deux matrices
peut être vu comme le produit scalaire de chaque ligne de la première matrice
avec chaque colonne de la deuxième matrice. *Note*: La compréhension de liste
pourrait aussi vous être utile.

## Exercice supplémentaire

Dans cette question, nous nous intéressons à l'implémentation d'une version
simplifiée du [jeu 2048](https://en.wikipedia.org/wiki/2048_(video_game)). Il
s'agit d'un jeu qui se joue seul, dans lequel on doit déplacer des tuiles,
représentées par les puissances de $2$. À chaque étape, nous pouvons faire
glisser toutes les tuiles dans une des quatre directions: à droite, à gauche,
vers le haut ou vers le bas. Lorsque des tuiles de même valeur entrent en
contact, elles sont fusionnées pour former une nouvelle tuile dont la valeur
est la somme des deux tuiles précédentes.

Complétez les 4 fonctions suivantes, données dans le fichier [`Jeu2048.hs`](./Jeu2048.hs):

```haskell
estTuile :: Int -> Bool
estValide :: Grille -> Bool
deplacerLigne :: [Tuile] -> [Tuile]
deplacer :: Direction -> Grille -> Grille
```

en faisant en sorte qu'elles respectent la documentation et les exemples qui
illustrent leur fonctionnement.
